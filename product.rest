POST http://localhost:4000/api/products/create
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNzI1ZDE3YWNiYjhkOWIxNjgzYWViMiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MTY2ODQ1MX0.pGVF4puwHDhlRoW2ycJdz9fZR-bKd2ur1Dkk9jCG_iw

{
    "name": "Buttered Toast",
    "description": "This tangy, salty, sour, love-it-or-hate-it yeast extract that turns a piece of grilled bread into a reason to go on living.",
    "price": 750,
    "image": "https://dynaimage.cdn.cnn.com/cnn/q_auto,w_634,c_fill,g_auto,h_357,ar_16:9/http%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F170302142757-buttered-toast-with-marmite.jpg"
}

###
POST https://vast-mountain-07825.herokuapp.com/api/products/create
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2I5NDRiYmJhZTBhN2EyODFmOTJiMyIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MjI2ODgwMn0.3vQTjJDTnJmnP8etRXvfZKNu2kph_iMvjQzO3wvmAHk

{
    "name": "Product 4",
    "description": "lorem ipsum 4",
    "price": 400
}

###
GET http://localhost:4000/api/products/

###
GET http://localhost:4000/api/products/active

###
GET https://vast-mountain-07825.herokuapp.com/api/products/

###
GET http://localhost:4000/api/products/629753de584953df5d847a32

###
GET https://vast-mountain-07825.herokuapp.com/api/products/62726ccf1ee3227d4ef052ae

###
PUT http://localhost:4000/api/products/archive/62975853584953df5d847a47
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2I5NDRiYmJhZTBhN2EyODFmOTJiMyIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MjI2ODgwMn0.3vQTjJDTnJmnP8etRXvfZKNu2kph_iMvjQzO3wvmAHk

###
PUT http://localhost:4000/api/products/activate/62975853584953df5d847a47
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2I5NDRiYmJhZTBhN2EyODFmOTJiMyIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MjI2ODgwMn0.3vQTjJDTnJmnP8etRXvfZKNu2kph_iMvjQzO3wvmAHk

###
PUT https://vast-mountain-07825.herokuapp.com/api/products/archive/62726d051ee3227d4ef052b1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNzI2M2ZmYjU1OWYyYzY3NjMyMzcwNCIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NTE2NjgyNzB9.I-BtL_5xWYu2TyY31a5AQVRLCN6Xkh5LhjYDAPI2Wrk

###
PUT http://localhost:3000/api/products/update/627b9fab2c514a58a181f233
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2I5NDRiYmJhZTBhN2EyODFmOTJiMyIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MjI2ODgwMn0.3vQTjJDTnJmnP8etRXvfZKNu2kph_iMvjQzO3wvmAHk

{
    "name": "Product 4-1",
    "description": "lorem ipsum 4-1",
    "price": 410
}

###
PUT https://vast-mountain-07825.herokuapp.com/api/products/update/627275f8572e093bf2a67366
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNzI1ZDE3YWNiYjhkOWIxNjgzYWViMiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MTY5ODQ3OH0.fKvd1QVIAUqTdjiVulzr0MF1JZWDUAI4E6ICpwUQvqo

{
    "name": "Product 4-1",
    "description": "lorem ipsum 4-1",
    "price": 1300,
    "isActive": false
}