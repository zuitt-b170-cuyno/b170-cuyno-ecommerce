require('dotenv').config()

const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())

const userRoute = require('./routes/user')
const orderRoute = require('./routes/order')
const productRoute = require('./routes/product')
const cartRoute = require('./routes/cart')

app.use('/api/users', userRoute)
app.use('/api/orders', orderRoute)
app.use('/api/products', productRoute)
app.use('/api/carts', cartRoute)

mongoose.connect("mongodb+srv://jhnnycyn:dxtr008X@cluster0.z5eip.mongodb.net/b170-ecommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.listen(process.env.PORT, () => console.log(`Server is now running at http//localhost:${process.env.PORT}`))