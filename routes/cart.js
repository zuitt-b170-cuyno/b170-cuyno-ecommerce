const express = require('express')
const auth = require('./../auth')
const CartController = require('./../controllers/cart')
const router = express.Router()

router.get('/', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    CartController.retrieveCart(token).then(result => res.send(result))
})

router.get('/all', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    CartController.retrieveAllCart(token).then(result => res.send(result))
})

router.post('/add', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const cart = { ...req.body, ...token }
    CartController.add(cart).then(result => res.send(result))
})

router.put('/purchase', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const cart = { ...token, product: req.body.product }
    CartController.purchaseCart(cart).then(result => res.send(result))
})

router.put('/remove', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const cart = { ...token, product: req.body.product }
    CartController.removeCart(cart).then(result => res.send(result))
})

module.exports = router