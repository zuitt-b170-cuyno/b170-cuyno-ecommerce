const express = require('express')
const auth = require('./../auth')
const OrderController = require('./../controllers/order')
const router = express.Router()

router.post('/checkout', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const order = { ...req.body, ...token }
    OrderController.checkout(order).then(result => res.send(result))
})

router.get('/', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    OrderController.retrieveAllOrders(token).then(result => res.send(result))
})

router.get('/:id', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const order = {
        userId: req.params.id,
        isAdmin: token.isAdmin
    }

    OrderController.retrieveUserOrders(order).then(result => res.send(result))
})

module.exports = router