const express = require('express')
const auth = require('./../auth')
const UserController = require('./../controllers/user')
const User = require('./../models/user')
const { check, validationResult } = require('express-validator')
const router = express.Router()

router.post('/checkEmail', (req, res) => UserController
    .checkEmail(req.body)
    .then(result => res.send(result)))

router.post('/register', [
    check('email')
        .notEmpty()
        .withMessage({
            message: 'Email cannot be empty',
            errorCode: 1
        })
        .isEmail()
        .withMessage({
            message: 'Invalid email',
            errorCode: 2
        })
        .custom(value => {
            return User.findOne({ email: value }).then(email => {
                if (email) return Promise.reject({
                    message: 'E-mail already in use',
                    errorCode: 3
                })
            })
        }), 
    check('password')
        .notEmpty()
        .withMessage({
            message: 'Password cannot be empty',
            errorCode: 4
        })
        .isLength({ min: 5 })
        .withMessage({
            message: 'Password must be at least 5 characters',
            errorCode: 5
        })
        .matches(/\d/)
        .withMessage({
            message: 'Password must contain a number',
            errorCode: 6
        })
        .isString()
        .withMessage({
            message: 'Invalid password',
            errorCode: 7
        })
], (req, res) => {
    const errors = validationResult(req)

    if (errors.isEmpty()) 
        UserController.register(req.body).then(result => res.send(result))
    else {
        const error = errors.array()[0]
        const message = error.msg
        return res.status(400).send({ error: message })
    }
})

router.post('/login', [
    check('email')
        .notEmpty()
        .withMessage({
            message: 'Email cannot be empty',
            errorCode: 1
        })
        .isEmail()
        .withMessage({
            message: 'Invalid email',
            errorCode: 2
        }),
    check('password')
        .notEmpty()
        .withMessage({
            message: 'Password cannot be empty',
            errorCode: 4
        })
], (req, res) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) 
        UserController.login(req.body).then(result => res.send(result))
    else {
        const error = errors.array()[0]
        const message = error.msg
        return res.status(400).send({ error: message })
    }
})

router.put('/set-admin/:id', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const user = { userId: req.params.id, ...token }
    UserController.setAdmin(user).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    UserController.getDetail(token).then(result => res.send(result))
})

module.exports = router