const express = require('express')
const auth = require('./../auth')
const ProductController = require('./../controllers/product')
const router = express.Router()

router.post('/create', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const product = { ...token, ...req.body }
    ProductController.createProduct(product).then(result => res.send(result))
})

router.get('/', (req, res) => {
    ProductController.retrieveAllProduct().then(result => res.send(result))
})

router.get('/active', (req, res) => {
    ProductController.retrieveActiveProduct().then(result => res.send(result))
})

router.get('/:id', (req, res) => {
    ProductController.retriveSpecificProduct(req.params.id).then(result => res.send(result))
})

router.put('/archive/:id', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const product = { productId: req.params.id, ...token }
    ProductController.archiveProduct(product).then(result => res.send(result))
})

router.put('/activate/:id', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const product = { productId: req.params.id, ...token }
    ProductController.activateProduct(product).then(result => res.send(result))
})

router.put('/update/:id', auth.verify, (req, res) => {
    const token = auth.decode(req.headers.authorization)
    const product = { 
        productId: req.params.id,
        ...token,
        ...req.body 
    }

    ProductController.updateProduct(product).then(result => res.send(result))
})

module.exports = router