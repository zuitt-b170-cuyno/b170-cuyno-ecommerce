const User = require('./../models/user')
const bcrypt = require('bcrypt')
const auth = require('./../auth')

module.exports.checkEmail = user => User
    .find({ email: user.email })
    .then((result, error) => error ? false : result.length > 0)

module.exports.register = detail => {
    const user = new User({ ...detail })
    user.password = bcrypt.hashSync(user.password, 10)
    return user.save().then((success, error) => {
        return {
            message: 'Registration successful',
            status: true,
        }
    })
}

module.exports.login = user => {
    return User.findOne({ email: user.email }).then(result => {
        if (!result) 
            return { 
                message: 'Bad credentials',
                status: false
            }
        else {
            const verification = bcrypt.compareSync(user.password, result.password)
            const access = { 
                message: 'Login successfully',
                status: true,
                access: auth.createAccessToken(result.toObject())
            }

            return verification ? access : { message: 'Bad credentials', status: false }
        }
    })
}

module.exports.setAdmin = user => {
    const { id: adminId,  userId, isAdmin } = user
    return User.findById(adminId).then(result => {
        if (!isAdmin)
            return { 
                message: 'Admin does not exist',
                status: false
            }
        else {
            return User.findById(userId).then((result, error) => {
                if (error) 
                    return { 
                        message: 'User does not exist',
                        status: false
                    }
                else {
                    result.isAdmin = true
                    return result.save().then((success, error) => { 
                        return {
                            message: 'Successfully updated',
                            status: true
                        }
                    })
                }
            })
        }
    })
}

module.exports.getDetail = user => {
    const { id: userId } = user
    return User.findById(userId).select('_id isAdmin').then(user => user)
}