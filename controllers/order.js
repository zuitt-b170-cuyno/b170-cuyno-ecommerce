const Order = require('./../models/order')
const User = require('./../models/user')

module.exports.checkout = detail => {
    const { id: userId, isAdmin, iat, ...product } = detail

    return User.findById(userId).then(result => {
        if (isAdmin)
            return false
        else {
            const { price, quantity, productId } = product
            const amount = price * quantity  
            const order = {
                totalAmount: amount,
                user: userId,
                product: productId
            }

            const newOrder = new Order(order)
            newOrder.save()
            return true
        }
    })
}

module.exports.retrieveUserOrders = detail => {
    const { isAdmin, userId } = detail
    return User.findById(userId).then(result => {
        if (isAdmin)
            return false
        else {
            return Order.find({ user: userId }, { _id: false })
                .populate('user', '-_id')
                .populate('product', 'name description price -_id')
                .select('-_id')
                .then(result => result)
        }
    })
}

module.exports.retrieveAllOrders = detail => {
    const { id: adminId, isAdmin } = detail

    return User.findById(adminId).then(result => {
        if (!isAdmin)
            return false
        else {
            return Order.find()
                .populate('user', '-_id')
                .populate('product', 'name description price -_id')
                .select('-_id')
                .then(result => result)
        }
    })
}