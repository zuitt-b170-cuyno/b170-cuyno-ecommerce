const Cart = require('./../models/cart')
const User = require('./../models/user')

module.exports.add = detail => {
    const { id: userId, isAdmin, iat, ...cart } = detail
    
    return User.findById(userId).then(() => {
        if (isAdmin)
            return false
        else {
            const newCart = new Cart({ user: userId, ...cart })
            return newCart.save().then((success, error) => !error)
        }
    })
}

module.exports.retrieveCart = detail => {
    const { id: userId, isAdmin } = detail
    
    return User.findById(userId).then(() => {
        if (isAdmin)
            return false
        else {
            return Cart.find({ user: userId, isActive: true })
                .populate('product', '-__v -image')
                .select('-_id -user -__v -createdOn')
                .then(cart => cart)
        }
    })
}

module.exports.retrievAllCart = detail => {
    const { id: userId, isAdmin } = detail
    
    return User.findById(userId).then(() => {
        if (!isAdmin)
            return false
        else {
            return Cart.find({ isActive: true })
                .populare('user', '-_id -__v')
                .populate('product', '-__v -image')
                .select('-_id -__v -createdOn')
                .then(cart => cart)
        }
    })
}

module.exports.purchaseCart = detail => {
    const { id: userId, isAdmin, product } = detail

    return User.findById(userId).then(() => {
        if (isAdmin)
            return false
        else {
            return Cart.find({ user: userId, product: product })
                .then(item => item[0])
                .then(item => {
                    item.isActive = false
                    item.isPurchased = true
                    item.save()
                    return true
                })
    }
    })
}

module.exports.removeCart = detail => {
    const { id: userId, isAdmin, product } = detail

    return User.findById(userId).then(() => {
        if (isAdmin)
            return false
        else {
            return Cart.find({ user: userId, product: product })    
                .then(item => item[0])
                .then(item => {
                    item.isActive = false
                    return item.save().then((success, error) => !error)
                })
        }
    })
}