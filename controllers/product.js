const Product = require('./../models/product')
const User = require('./../models/user')

module.exports.createProduct = detail => {
    const { id: adminId, isAdmin, ...product } = detail

    return User.findById(adminId).then(result => {
        if (!isAdmin)
            return false
        else {
            const newProduct = new Product(product)
            return newProduct.save().then((success, error) => !error)
        }
    })    
}

module.exports.retrieveAllProduct = () => {
    return Product.find().select('-__v').then(result => result)
}

module.exports.retrieveActiveProduct = () => {
    return Product.find({ isActive: true }).select('-__v').then(result => result)
}

module.exports.retriveSpecificProduct = id => {
    return Product.findById(id).select('-__v').then(result => result)
}

module.exports.archiveProduct = detail => {
    const { id: adminId, isAdmin, productId } = detail

    return User.findById(adminId).then(result => {
        if (!isAdmin)
            return false
        else {
            return Product.findById(productId).then(result => {
                result.isActive = false
                return result.save().then((success, error) => !error)
            })
        }
    })
}

module.exports.activateProduct = detail => {
    const { id: adminId, isAdmin, productId } = detail

    return User.findById(adminId).then(result => {
        if (!isAdmin)
            return false
        else {
            return Product.findById(productId).then(result => {
                result.isActive = true
                return result.save().then((success, error) => !error)
            })
        }
    })
}

module.exports.updateProduct = detail => {
    const { id: adminId, iat, isAdmin, productId, ...product } = detail

    return User.findById(adminId).then(result => {
        if (!isAdmin)
            return false
        else {
            return Product.findById(productId).then(result => {
                const keys = Object.keys(product)
                keys.forEach(key => {
                    if (product[key])
                        result[key] = product[key]
                })
                return result.save().then((success, error) => !error)
            })
        }
    })
}