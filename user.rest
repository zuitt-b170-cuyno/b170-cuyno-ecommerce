POST http://localhost:3000/api/users/register 
Content-Type: application/json

{
    "email": "jhnnycyn@mail.com",
    "password": "1111"
}

###
POST https://vast-mountain-07825.herokuapp.com/api/users/register 
Content-Type: application/json

{
    "email": "jander@mail.com",
    "password": "33333"
}

###
POST http://localhost:4000/api/users/login
Content-Type: application/json

{
    "email": "faye@mail.com",
    "password": "22222"
}

###
POST https://vast-mountain-07825.herokuapp.com/api/users/login
Content-Type: application/json

{
    "email": "faye@mail.com",
    "password": "22222"
}

###
PUT http://localhost:3000/api/users/set-admin/62726413b559f2c676323706
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNzI1ZDE3YWNiYjhkOWIxNjgzYWViMiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MTY2NDA2N30.leeq7RVMbL5jklcwJNGV2lLzk_4rCLQTajmJAMEK-Sg

###
PUT https://vast-mountain-07825.herokuapp.com/api/users/set-admin/627b949fbbae0a7a281f92ba
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2I5NDRiYmJhZTBhN2EyODFmOTJiMyIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1MjI2ODgwMn0.3vQTjJDTnJmnP8etRXvfZKNu2kph_iMvjQzO3wvmAHk

###
GET http://localhost:4000/api/users/details
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2I5NDViYmJhZTBhN2EyODFmOTJiNiIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NTQzMjc0MzF9.jquW9aLOamGLJwgiKsl7MJes-HN3uWCgM62HxIMhrBw

###
POST http://localhost:4000/api/users/checkEmail
Content-Type: application/json

{
    "email": "jhnnycyn@mail.com"
}