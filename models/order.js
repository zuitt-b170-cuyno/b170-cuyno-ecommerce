const mongoose = require('mongoose')
const { Schema } = mongoose
const { ObjectId } = mongoose.Schema.Types

const schema = new Schema({
    totalAmount: {
        type: Number,
        required: [true, "Total amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    user: {
        type: ObjectId,
        ref: 'User'
    },
    product: {
        type: ObjectId,
        ref: 'Product'
    }
})

module.exports = mongoose.model('Order', schema)