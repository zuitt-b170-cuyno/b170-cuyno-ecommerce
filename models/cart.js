const mongoose = require('mongoose')
const { Schema } = mongoose
const { ObjectId } = mongoose.Schema.Types

const schema = new Schema({
    isPurchased: {
        type: Boolean,
        default: false
    },
    cartOn: {
        type: Date,
        default: new Date()
    },
    quantity: {
        type: Number,
        required: [true, "Quantity is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    user: {
        type: ObjectId,
        ref: 'User'
    },
    product: {
        type: ObjectId,
        ref: 'Product'
    }
})

module.exports = mongoose.model('Cart', schema)