const mongoose = require('mongoose')
const { Schema } = mongoose

const schema = new Schema({
    name: {
        type: String,
        required: [true, "Name of product is required"]
    },
    description: String,
    price: {
        type: Number, 
        required: [true, "Price is required"]
    },
    isActive: {
        type: Boolean, 
        default: true
    },
    image: {
        type: String,
        required: [true, "Image is required"]
    },
    createdOn: {
        type: Date, 
        default: new Date()
    }
})

module.exports = mongoose.model('Product', schema)