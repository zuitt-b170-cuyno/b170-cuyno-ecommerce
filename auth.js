const jwt = require('jsonwebtoken')

module.exports.createAccessToken = user => {
    const data = { 
        id: user._id,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, process.env.ACCESS_TOKEN_SECRET)
}

module.exports.verify = (req, res, next) => {
    const header = req.headers.authorization
    const token = header && header.split(' ')[1]

    if (token) {
        return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, user) => {
            if (error) 
                return res.send({ auth: "Failed" })
            next()
        })
    }
}

module.exports.decode = header => {
    const token = header && header.split(' ')[1]

    if (token) 
        return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, data) => 
            error ? null : jwt.decode(token, { complete: true }).payload)
}